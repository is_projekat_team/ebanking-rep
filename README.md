# Elektronsko bankarstvo
Analiza i projektovanje sistema za elektronsko placanje koje obezbedjuje korisnicima razne vrste finansijskih transakcija kroz online portal finansijske institucije.

## Tim:
Andja Ranisavljev - koordinator za implementaciju prototipa
Nikola Grulovic - koordinator za modeliranje baze podataka
Tomislav Milovanovic - koordinator za osmisljavanje korisnickog interfejsa
Petar Micic - koordinator za pravljenje arhitekture
 
## LaTex dokument: https://www.overleaf.com/4186874554fnymgzpjbrxz
